# vim
## .vimrc.bepo
Configuration sur la base de description du [wiki](https://bepo.fr/wiki/Vim) inchangée pour le moment.
```shell
echo "source git/configurationsbepo/vim/.vimrc.bepo" >> .vimrc
```
