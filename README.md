# Disposition bépo
## Qu'est-ce que c'est ?
La disposition de clavier [bépo](http://bepo.fr/wiki/Accueil] est fantastique.
Contrairement azerty la disposition bépo a été créé pour la morphologie humaine.
Cependant, la plupart des logiciels historiques tel que vim ont été pensés pour
une disposition qwerty (clavier historique) américain. Leur utilisation 
nécessite donc quelques réglages.
![Disposition bépo](https://download.tuxfamily.org/dvorak/wiki/images/Bepo-1.1-complet.png)

Pour ma part, j'utilise également un clavier TypeMatrix encore plus ergonomique.
![Disposition TypeMatrix](http://www.typematrix.com/images/2009-skins/035-b_french-france-bepo_1000x429.png)

## Pourquoi ?
La santé !
Avec le temps beaucoup de ceux qui travaillent sur ordinateur finissent par 
avoir des douleurs aux doigts/mains/avant-bras/épaules selon les cas. Chercher
dans un moteur de recherche «troubles musculo-squeléttique» aka «TMS» ou en 
anglais «Repetitive Strain Injury» «RSI».
Le sport / exercices ainsi qu'une bonne ergonomie de travail aide beaucoup.
